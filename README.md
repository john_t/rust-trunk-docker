# Rust Trunk

A simple Docker image to build [Rust](https://www.rust-lang.org) [Wasm](https://webassembly.org/)
apps with [Trunk](https://trunkrs.dev/) - a low configuration build tool.

Licensed under MIT/APACHE-2.0
